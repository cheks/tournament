package main

import (
	"gopkg.in/mgo.v2"
	"sync"
	"time"
	"reflect"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/night-codes/types.v1"
	"math/rand"
	"errors"
)

type (
	TournamentStruct struct {
		ID       uint64            `form:"tournamentId" bson:"_id"`
		Deposit  float64           `form:"deposit" bson:"deposit"`
		Finished bool              `bson:"finished"`
		Members  map[string]Member `bson:"members"`
		Jackpot  float64           `bson:"jackpot"`
	}

	Member struct {
		PlayerID uint64   `bson:"playerId"`
		Type     string   `bson:"type"`
		Amount   float64  `bson:"amount"`
		Rate     float64  `bson:"rate"`
		Backers  []uint64 `bson:"backers,omitempty"`
	}

	TournamentCollection struct {
		Collection *mgo.Collection
	}

	ResultCollection struct {
		Collection *mgo.Collection
	}

	TournCacheStruct struct {
		data map[uint64]TournamentStruct
		sync.RWMutex
	}

	TournamentResult struct {
		TournID  uint64  `bson:"tournamentId"`
		PlayerId uint64  `bson:"playerId"`
		Prize    float64 `bson:"prize"`
	}
)

var (
	TournCache = TournCacheStruct{}
	Tournament = TournamentCollection{Collection: mongo.C("tournaments")}
	Result     = ResultCollection{Collection: mongo.C("results")}
)

func init() {

	TournCache.data = make(map[uint64]TournamentStruct)
	TournCache.Tick()

	RunTournament()

	go func() {
		for range time.Tick(time.Second * 5) {
			TournCache.Tick()
		}
	}()

	go func() {
		for range time.Tick(time.Minute * 3) {
			RunTournament()
		}
	}()
}

func (a *TournCacheStruct) Tick() {

	tourns := []TournamentStruct{}

	err := Tournament.Collection.Find(obj{"finished": false}).All(&tourns)
	if err != nil {
		Error.Println(err)
	}

	data := make(map[uint64]TournamentStruct)
	for i := range tourns {
		tmp := tourns[i]
		data[tmp.ID] = tmp
	}
	a.Lock()
	a.data = data
	a.Unlock()

}

func (a *TournamentResult) SaveResult() error {

	err := Result.Collection.Insert(a)

	return err
}

func GetResults() (obj, error) {

	res := obj{}

	err := Result.Collection.Pipe([]bson.M{
		{"$group":
		bson.M{"_id": nil, "winners":
		bson.M{"$push": bson.M{"playerId": "$playerId", "prize": "$prize"}}}},
		{"$project": bson.M{"_id": 0}},
	}).One(&res)

	return res, err

}

func JoinSoloToTournament(tourn TournamentStruct, player PlayerStruct) error {

	player.Amount -= tourn.Deposit

	if err := player.UpdateBalance(); err != nil {
		return err
	}

	strId := types.String(player.ID)
	if _, ok := tourn.Members[strId]; ok {
		return errors.New("Player already joined")
	}

	member := Member{}
	member.PlayerID = player.ID
	member.Type = "solo"
	member.Amount = tourn.Deposit
	member.Rate = 1

	tourn.Jackpot += tourn.Deposit

	tourn.Members[strId] = member

	if err := tourn.UpdateMembers(); err != nil {
		player.Points += tourn.Deposit
		tourn.Jackpot -= tourn.Deposit
		delete(tourn.Members, strId)
		return err
	}

	PlayersCache.Lock()
	PlayersCache.data[player.ID] = player
	PlayersCache.Unlock()

	TournCache.Lock()
	TournCache.data[player.ID] = tourn
	TournCache.Unlock()

	return nil

}

func (a *TournamentStruct) UpdateMembers() error {

	err := Tournament.Collection.Update(obj{"_id": a.ID}, obj{
		"$set": obj{"members": a.Members},
		"$inc":obj{"jackpot": a.Deposit}})


	return err
}

func (a *TournamentStruct) Finish(winner Member) error {

	var amount float64

	if winner.Type == "solo" {
		amount = a.Jackpot
	} else {
		amount = a.Jackpot * (winner.Rate / 100)
	}

	winners := winner.Backers
	winners = append(winners, winner.PlayerID)

	for _, id := range winners {
		PlayersCache.RLock()
		player := PlayersCache.data[id]
		PlayersCache.RUnlock()

		player.Amount = amount
		err := player.UpdateBalance()
		if err != nil {

			break
			return err
		}

		PlayersCache.Lock()
		PlayersCache.data[player.ID] = player
		PlayersCache.Unlock()
	}

	err := Tournament.Collection.Update(obj{"_id": a.ID}, obj{"$set": obj{"finished": true}})
	if err != nil {
		reBalance(winners, amount)
		return err
	}

	winPlayer := obj{}
	winPlayer["playerId"] = winner.PlayerID
	winPlayer["prize"] = a.Jackpot

	result := TournamentResult{}
	result.TournID = a.ID
	result.PlayerId = winner.PlayerID
	result.Prize = a.Jackpot

	err = result.SaveResult()
	if err != nil {
		reBalance(winners, amount)
		Tournament.Collection.Update(obj{"_id": a.ID}, obj{"$set": obj{"finished": false}})
		return err
	}

	return nil
}

func JoinMultiToTournament(playerArr []uint64, neededSum float64, player PlayerStruct, backers []uint64, tournament TournamentStruct) error {

	rate := 100 / float64(len(backers)+1)
	errs := []uint64{}

	for _, id := range playerArr {
		PlayersCache.RLock()
		player := PlayersCache.data[id]
		PlayersCache.RUnlock()

		player.Amount -= neededSum
		err := player.UpdateBalance()
		if err != nil {
			errs = append(errs, player.ID)
		} else {
			PlayersCache.Lock()
			PlayersCache.data[player.ID] = player
			PlayersCache.Unlock()
		}
	}

	if len(errs) > 0 {
		reBalance(errs, neededSum)
		return errors.New("error change player balance")
	}

	member := Member{}
	member.PlayerID = player.ID
	member.Type = "multi"
	member.Amount = neededSum
	member.Rate = rate
	member.Backers = backers

	tournament.Jackpot += tournament.Deposit
	tournament.Members[types.String(player.ID)] = member

	if err := tournament.UpdateMembers(); err != nil {
		reBalance(errs, neededSum)
		return err
	}

	TournCache.Lock()
	TournCache.data[player.ID] = tournament
	TournCache.Unlock()

	return nil
}

func RunTournament() {

	TournCache.Lock()
	tournaments := TournCache.data
	TournCache.Unlock()

	if len(tournaments) > 0 {
		for _, tourn := range tournaments {
			if len(tourn.Members) > 0 {

				keys := reflect.ValueOf(tourn.Members).MapKeys()

				rand.Seed(time.Now().Unix())
				i := types.String(keys[rand.Intn(len(keys))])
				winner := tourn.Members[i]

				err := tourn.Finish(winner)
				if err != nil {
					Error.Println(err)
				}
			}
		}
	}
}

func tournamentIsExist(id uint64) (TournamentStruct, bool) {

	TournCache.RLock()
	data, ok := TournCache.data[id]
	TournCache.RUnlock()

	return data, ok
}
