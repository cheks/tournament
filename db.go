package main

import (
	"gopkg.in/mgo.v2"
)

const (
	DBName = "tournament"
)

var (
	mongo = DB()
)

func DB() *mgo.Database {

	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

	session.SetMode(mgo.Monotonic, true)

	return session.DB(DBName)
}
