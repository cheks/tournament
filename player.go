package main

import (
	"gopkg.in/mgo.v2"
	"sync"
	"time"
)

type (
	PlayerStruct struct {
		ID     uint64  `form:"playerId" json:"playerId" bson:"_id"`
		Points float64 `form:"points" json:"balance" bson:"points"`
		Amount float64 `form:"-" json:"-" bson:"-"`
	}

	PlayerCollection struct {
		Collection *mgo.Collection
	}

	PlayersCacheStruct struct {
		data map[uint64]PlayerStruct
		sync.RWMutex
	}
)

var (
	PlayersCache = PlayersCacheStruct{}
	Player       = PlayerCollection{Collection: mongo.C("players")}
)

func init() {

	PlayersCache.data = make(map[uint64]PlayerStruct)
	PlayersCache.Tick()

	go func() {
		for range time.Tick(time.Second * 5) {
			PlayersCache.Tick()
		}
	}()

}

func (a *PlayersCacheStruct) Tick() {

	players := []PlayerStruct{}
	err := Player.Collection.Find(obj{}).All(&players)
	if err != nil {
		Error.Print(err)
	}

	data := make(map[uint64]PlayerStruct)
	for i := range players {
		tmp := players[i]
		data[tmp.ID] = tmp
	}
	a.Lock()
	a.data = data
	a.Unlock()

}

func (a *PlayerStruct) UpdateBalance() error {

	err := Player.Collection.Update(obj{"_id": a.ID}, obj{"$inc": obj{"points": a.Amount}})

	return err
}

func playerIsExist(id uint64) (PlayerStruct, bool) {

	PlayersCache.RLock()
	data, ok := PlayersCache.data[id]
	PlayersCache.RUnlock()

	return data, ok
}

func checkPlayerForJoin(ids []uint64, neededSum float64) bool {

	for _, id := range ids {
		PlayersCache.RLock()
		player, ok := PlayersCache.data[id]
		PlayersCache.RUnlock()
		if !ok {
			return false
		}

		if player.Points < neededSum {
			return false
		}

	}

	return true
}

func reBalance(ids []uint64, amount float64) {

	for _, id := range ids {
		PlayersCache.RLock()
		player := PlayersCache.data[id]
		PlayersCache.RUnlock()

		player.Amount = amount
		if err := player.UpdateBalance(); err != nil {
			Error.Println(err)
		} else {
			PlayersCache.Lock()
			PlayersCache.data[player.ID] = player
			PlayersCache.Unlock()
		}
	}

}
