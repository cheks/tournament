package main

import (
	"gopkg.in/gin-gonic/gin.v1"
	"gopkg.in/gin-gonic/gin.v1/binding"
	"errors"
	"gopkg.in/night-codes/types.v1"
)

type (
	obj map[string]interface{}

	JoinTournament struct {
		ID       uint64   `form:"tournamentId"`
		PlayerID uint64   `form:"playerId"`
		Backers  []uint64 `form:"backerId"`
	}
)

func main() {

	router := gin.Default()

	// списание средств с баланса +
	router.GET("/take", func(c *gin.Context) {

		acc := PlayerStruct{}
		if err := bindValid(c, &acc); err != nil {
			c.String(404, err.Error())
			return
		}

		PlayersCache.RLock()
		player, ok := PlayersCache.data[acc.ID]
		PlayersCache.RUnlock()

		if !ok {
			c.String(404, "Player error: not found")
			return
		}

		if acc.Points > player.Points {
			player.Amount = acc.Points - player.Points
		} else {
			player.Amount -= acc.Points
		}

		if err := player.UpdateBalance(); err != nil {
			c.String(404, "Player error: update balance error")
			return
		}

		PlayersCache.Lock()
		PlayersCache.data[player.ID] = player
		PlayersCache.Unlock()

		c.String(200, "")
	})

	// пополнить баланс пользователя || создать если не существует +
	router.GET("/fund", func(c *gin.Context) {

		acc := PlayerStruct{}

		if err := bindValid(c, &acc); err != nil {
			c.String(404, err.Error())
			return
		}

		PlayersCache.Lock()
		player, ok := PlayersCache.data[acc.ID]
		PlayersCache.Unlock()

		if !ok {
			if err := Player.Collection.Insert(acc); err != nil {
				Error.Println(err.Error())
				c.String(404, "Player create error")
				return
			} else {

				PlayersCache.Lock()
				PlayersCache.data[acc.ID] = acc
				PlayersCache.Unlock()

				c.String(200, "")
				return
			}
		}

		player.Amount = acc.Points
		if err := player.UpdateBalance(); err != nil {
			c.String(404, "Player error: update balance error")
			return
		}

		PlayersCache.Lock()
		PlayersCache.data[acc.ID] = acc
		PlayersCache.Unlock()

		c.String(200, "")
	})

	// объявить турнир +
	router.GET("/announceTournament", func(c *gin.Context) {

		tournament := TournamentStruct{}

		if err := c.MustBindWith(&tournament, binding.Form); err != nil {
			Error.Println(err)
			c.String(404, "Wrong query params")
			return
		}

		if tournament.ID == 0 || tournament.Deposit == 0 {
			c.String(404, "Wrong query params")
			return
		}

		TournCache.Lock()
		_, ok := TournCache.data[tournament.ID]
		TournCache.Unlock()

		if ok {
			c.String(404, "Tournament already announced")
			return
		}

		if err := Tournament.Collection.Insert(tournament); err != nil {
			Error.Println(err)
			c.String(404, "Create tournament error")
			return
		}

		TournCache.Lock()
		TournCache.data[tournament.ID] = tournament
		TournCache.Unlock()

		c.String(200, "")
	})

	// присоеденится к турниру
	router.GET("/joinTournament", func(c *gin.Context) {

		join := JoinTournament{}

		c.MustBindWith(&join, binding.Form)

		tournament, ok := tournamentIsExist(join.ID)

		if !ok {
			c.String(404, "Tournament does not exist")
			return
		}

		player, ok := playerIsExist(join.PlayerID)
		if !ok {
			c.String(404, "Player does not exist")
			return
		}

		strId := types.String(player.ID)

		if _, ok := tournament.Members[strId]; ok {
			c.String(404, "Player already joined")
			return
		}

		switch {
		case len(join.Backers) == 0:

			if player.Points < tournament.Deposit {
				c.String(404, "insufficient funds for join to tournament")
				return
			}

			err := JoinSoloToTournament(tournament, player)

			if err != nil {
				Error.Println(err)
				c.String(200, "join tournament entry error")
				return
			}
		case len(join.Backers) > 0:

			if player.Points == 0 {
				c.String(404, "insufficient funds for join to tournament")
				return
			}

			neededSum := tournament.Deposit / float64(len(join.Backers)+1)

			playerArr := join.Backers
			playerArr = append(playerArr, join.PlayerID)

			if !checkPlayerForJoin(playerArr, neededSum) {
				c.String(404, "insufficient funds for join to tournament or some player does not exist")
				return
			}

			err := JoinMultiToTournament(playerArr, neededSum, player, join.Backers, tournament)

			if err != nil {
				Error.Println(err)
				c.String(200, "join tournament entry error")
				return
			}

			c.String(200, "")

		default:
			c.String(404, "join tournament entry error")
		}
	})

	router.GET("/resultTournament", func(c *gin.Context) {

		result, err := GetResults()
		if err != nil {
			if err.Error() != "not found" {
				Error.Println(err)
			}
		}

		c.JSON(200, result)

	})

	//Баланс пользователя +
	router.GET("/balance", func(c *gin.Context) {

		acc := PlayerStruct{}

		if err := c.MustBindWith(&acc, binding.Form); err != nil {
			Error.Println(err.Error())
			c.String(404, "Wrong query params. playerId must be a NUMBER")
			return
		}

		PlayersCache.RLock()
		player, ok := PlayersCache.data[acc.ID]
		PlayersCache.RUnlock()

		if !ok {
			c.String(404, "Player error: not found")
			return
		}

		c.JSON(200, player)
	})

	// сбросить данные в бд +
	router.GET("/reset", func(c *gin.Context) {

		_, err := Player.Collection.RemoveAll(obj{})

		if err != nil {
			Error.Println(err)
		}

		_, err = Tournament.Collection.RemoveAll(obj{})

		if err != nil {
			Error.Println(err)
		}

		_, err = Result.Collection.RemoveAll(obj{})

		if err != nil {
			Error.Println(err)
		}

		c.String(200, "")
	})

	// заглушка +
	router.GET("/", func(c *gin.Context) {
		c.String(404, "wrong path")
	})

	Info.Println("Server started at http://localhost:4848")
	panic(router.Run(":4848"))

	if r := recover(); r != nil {
		Error.Println("Recovered in r", r)
	}
}

func bindValid(c *gin.Context, acc *PlayerStruct) error {
	if err := c.MustBindWith(acc, binding.Form); err != nil {
		return errors.New("Wrong query params")
	}

	if acc.ID == 0 || acc.Points == 0 {
		return errors.New("Wrong query params")
	}
	return nil
}
